#include <stdio.h>
#include <stdlib.h>
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"



int main(int argc, char** argv) {
    int width, height, channels;

    // reading image from disk as greyscale
    unsigned char* rgb_image = stbi_load(argv[1], &width, &height, &channels, 0);
    if (rgb_image == NULL) {
        printf("Error Loading Image from disk \n");
        exit(1);
    }
    printf("Image Loaded with height = %dpx, width = %dpx, and channels = %dpx\n", width, height, channels);


    // RGB -> Gray -> Negative
    size_t rgb_image_size = width * height * channels;
    int gray_channels = channels == 4? 2 : 1;
    size_t gray_image_size = width * height * gray_channels;
    unsigned char* gray_neg_image = malloc(gray_image_size);
    if (gray_neg_image == NULL) {
        printf("Unable to allocate memory for gray image.\n");
        exit(1);
    }
    printf("Memory Allocated for Gray Negative Image.\n");

    for (
        unsigned char *rgb=rgb_image, *gray=gray_neg_image; 
        rgb != rgb_image + rgb_image_size; 
        rgb += channels, gray += gray_channels 
    ) {
        // Calculate GrayScale Value - (r+g+b)/3
        *gray = (uint8_t) ((*rgb + *(rgb+1) + *(rgb+2))/3.0);
        // Calculate Negative Value - (255 - gray)
        *gray = (unsigned char) (255.0 - *gray);
        if (channels == 4) {
            *(gray + 1) = *(rgb + 3);
        }
    }


    // storing the image on disk
    stbi_write_jpg(argv[2], width, height, gray_channels, gray_neg_image, 100);
    
    stbi_image_free(rgb_image);
    stbi_image_free(gray_neg_image);

}