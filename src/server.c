#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

#define SIZE 1024

void recieve_request(int sockfd){

    char image_path[SIZE];
    int n;


    while (1) {
        n = recv(sockfd, image_path, SIZE, 0);
        if (n <= 0){
            break;
            return;
        }

    int width, height, channels;

    // reading image from disk as greyscale
    unsigned char* rgb_image = stbi_load(image_path, &width, &height, &channels, 0);
    if (rgb_image == NULL) {
        printf("Error Loading Image from disk : %s \n", image_path);
        exit(1);
    }
    printf("Image Loaded %s with height = %dpx, width = %dpx, and channels = %dpx\n", image_path, width, height, channels);


    // RGB -> Gray -> Negative
    size_t rgb_image_size = width * height * channels;
    int gray_channels = channels == 4? 2 : 1;
    size_t gray_image_size = width * height * gray_channels;
    unsigned char* gray_neg_image = malloc(gray_image_size);
    if (gray_neg_image == NULL) {
        printf("Unable to allocate memory for gray image.\n");
        exit(1);
    }
    printf("Memory Allocated for Gray Negative Image.\n");

    for (
        unsigned char *rgb=rgb_image, *gray=gray_neg_image; 
        rgb != rgb_image + rgb_image_size; 
        rgb += channels, gray += gray_channels 
    ) {
        // Calculate GrayScale Value - (r+g+b)/3
        *gray = (uint8_t) ((*rgb + *(rgb+1) + *(rgb+2))/3.0);
        // Calculate Negative Value - (255 - gray)
        *gray = (unsigned char) (255.0 - *gray);
        if (channels == 4) {
            *(gray + 1) = *(rgb + 3);
        }
    }


    // storing the image on disk
    stbi_write_jpg("temp/server_out.jpg", width, height, gray_channels, gray_neg_image, 100);
    
    stbi_image_free(rgb_image);
    stbi_image_free(gray_neg_image);
    }
    return;
}

int main(){
  char *ip = "127.0.0.1";
  int port = 8080;
  int e;

  int sockfd, new_sock;
  struct sockaddr_in server_addr, new_addr;
  socklen_t addr_size;

  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if(sockfd < 0) {
    perror("[-]Error in socket");
    exit(1);
  }
  printf("[+]Server socket created successfully.\n");

  server_addr.sin_family = AF_INET;
  server_addr.sin_port = port;
  server_addr.sin_addr.s_addr = inet_addr(ip);

  e = bind(sockfd, (struct sockaddr*)&server_addr, sizeof(server_addr));
  if(e < 0) {
    perror("[-]Error in bind");
    exit(1);
  }
  printf("[+]Binding successfull.\n");

  if(listen(sockfd, 10) == 0){
 printf("[+]Listening....\n");
 }else{
 perror("[-]Error in listening");
    exit(1);
 }

  addr_size = sizeof(new_addr);
  new_sock = accept(sockfd, (struct sockaddr*)&new_addr, &addr_size);
  recieve_request(new_sock);
  printf("[+]Data written in the file successfully.\n");

  return 0;
}